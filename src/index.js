const http = require('http');
const fs = require('fs');
const { v4: uuidv4 } = require('uuid');
const statusCodeFile = require("http").STATUS_CODES;


const server = http.createServer((req, res) => {

    if (req.method === 'GET') {

        const URL = req.url;

        if (URL === '/html') {

            res.writeHead(200, { 'Content-Type': 'text/html' });
            const htmlData = fs.readFileSync("../data/quote.html");
            res.end(htmlData);

        }
        else if (URL === '/json') {

            res.writeHead(200, { 'Content-Type': 'application/json' });
            const jsonData = fs.readFileSync("../data/jsonData.json");
            res.end(jsonData);

        }
        else if (URL === '/uuid') {

            res.writeHead(200, { 'Content-Type': 'application/json' });
            const uuIdObj = {
                uuid: uuidv4()
            }
            res.end(JSON.stringify(uuIdObj));

        }
        else if (URL.match('/status/\?')) {

            res.writeHead(200, { 'Content-Type': 'text/plain' });
            const statusCode = URL.split('/')[2];

            if (statusCodeFile[statusCode]) {

                res.end(statusCodeFile[statusCode]);
            }
            else {
                res.end("Invalid Status Code!");
            }

        }
        else if (URL.match('/delay/\?')) {

            res.writeHead(200, { 'Content-Type': 'text/plain' });
            const delayTime = Number(URL.split('/')[2]);

            if ((typeof delayTime === 'number') && (delayTime !== NaN)) {

                setTimeout(() => {
                    res.end(statusCodeFile[200]);
                }, delayTime * 1000);
            }
            else {
                res.end("Invalid Delay Time!");
            }
        }
        else{
            res.writeHead(404);
            res.end("Invalid URL!");
        }
    }

});


server.listen(8080);